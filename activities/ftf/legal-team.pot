#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: 33823\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-14 21:12+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#: activities/ftf/legal-team.en.xhtml+html.head.title:9
msgid "The Legal Network - FSFE Legal"
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.head:-1
msgid ""
"Information about the largest structure for legal discussion on Free "
"Software in the world."
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.head:-1
msgid ""
"Legal Network, LN, European Legal Network, ELN, Free Software, Open Source, "
"licensing, FSFE, GPL, copyleft, licence violation, lawyers, law, legal "
"experts, best practices, information, join, aim, scope, introduction"
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.p:21
msgid "Legal activities"
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.h1:23
msgid "The Legal Team of the FSFE"
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.p:25
msgid ""
"The Legal Team of the FSFE is a group of volunteers with legal expertise "
"that helps the FSFE with its <a href=\"/activities/ftf/activities.html\">"
"legal activities</a>."
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.p:27
msgid ""
"The most crucial role of FSFE’s Legal Team is to provide advice and opinions "
"over legal policy aspects and to manage our <a href=\"/activities/ftf/fla."
"html\">Fiduciary Licence Agreement</a>. Most of the Legal Team members are "
"also part of our <a href=\"/activities/ftf/licence-questions.html\">licence "
"questions list</a> and of the <a href=\"/activities/ftf/ln.html\">Legal "
"Network</a> contributing to spreading knowledge on the legal aspects of Free "
"Software through those channels."
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.p:29
msgid ""
"Furthermore, the FSFE’s Legal Team is charged with managing the <a href=\""
"/activities/ftf/ln.html\">Legal Network</a>, specifically:"
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.ul.li:32
msgid ""
"organising the <a href=\"/activities/ftf/legal-conference.html\">Free "
"Software Legal and Licensing Workshop</a>"
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.ul.li:34
msgid ""
"accepting <a href=\"/activities/ftf/ln-rules.html#join\">new members</a>"
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.ul.li:36
msgid ""
"acting upon breaches of the Legal Network <a href=\"/activities/ftf/ln-"
"rules.html#breaches\">regulations</a>."
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.h2:39
msgid "Contacts"
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.p:41
msgid ""
"You can reach the FSFE’s Legal Team at <a href=\"mailto:legal@lists.fsfe."
"org\">legal@lists.fsfe.org</a>. Please contact the FSFE’s Legal Team only if "
"you have a question regarding the Legal Network or a legal question that "
"involves the FSFE directly."
msgstr ""

#: activities/ftf/legal-team.en.xhtml+html.body.p:43
msgid ""
"If you want to become an active volunteer on our Legal Team you can contact "
"the <a href=\"/about/team.html\">Legal Team Coordinator</a>."
msgstr ""
